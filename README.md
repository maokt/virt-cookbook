virt Cookbook
=============

Installs libvirt and qemu-kvm if this machine is a host, and not a guest,
according to the virtualization information discovered by Ohai.
