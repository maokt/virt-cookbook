case node["virtualization"]["role"]
when "host"
    include_recipe "virt::vagrant"
    include_recipe "virt::docker"
when "guest"
    log "nothing to do for virtual guest (yet)"
else
    log "no virtualisation"
end
